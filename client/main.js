import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

let api = "https://jsonplaceholder.typicode.com/photos";
let i = 0;

Tiles = new Mongo.Collection('tiles', {connection: null});

Template.something.onCreated(function() {
  let tempApi = api + "?_start=" + String(i) + "&_end=" + String(i + 3);
  $.getJSON(tempApi, function(data) {
    Tiles.insert(
      {Cid: data[0].id, title: data[0].title, ImgSrc: data[0].url}
    );
    Tiles.insert(
      {Cid: data[1].id, title: data[1].title, ImgSrc: data[1].url}
    );
    Tiles.insert(
      {Cid: data[2].id, title: data[2].title, ImgSrc: data[2].url}
    );
  });
});

Template.something.helpers({
  'block': function() {
      return Tiles.find();
  }
});

Template.something.events({
  'click #loadmore': function() {
    i += 3;
    let tempApi = api + "?_start=" + String(i) + "&_end=" + String(i + 3);
    $.getJSON(tempApi, function(data) {
      Tiles.insert(
        {Cid: data[0].id, title: data[0].title, ImgSrc: data[0].url}
      );
      Tiles.insert(
        {Cid: data[1].id, title: data[1].title, ImgSrc: data[1].url}
      );
      Tiles.insert(
        {Cid: data[2].id, title: data[2].title, ImgSrc: data[2].url}
      );
    });
  },
  'click #fullsize': function() {
    let modal = document.getElementById('myModal');
    let modalImg = document.getElementById("img01");
    let captionText = document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = this.ImgSrc;
    captionText.innerHTML = this.title;
  },
  'click .close': function() {
    $("#myModal").css("display", "none");
  },
  'mouseenter .card': function(event) {
    $(event.target).find("i").removeClass("fa-angle-up").addClass("fa-angle-down");
  },
  'mouseleave .card': function(event) {
    $(event.target).find("i").removeClass("fa-angle-down").addClass("fa-angle-up");
  }
});
